import React from 'react'; 
import './App.css';
import Workflow from './Components/Workflow';
import Person from './Components/Person';
import Communication from './Components/Communication';
import Final from './Components/Final';
class App extends React.Component {

  state = {
    showPersonal: true,
    showCommunication: false,
    showFinal:false,
    showWorkflow:true
  
  }

  handleClick(arg) {  
   
    if( arg === 'personal'){
        this.setState({
            showPersonal: true ,
            showCommunication: false,
          
        });
    }else if( arg === 'commun' ){
        this.setState({
            showPersonal: false ,
            showCommunication: true
        });
    }
    
  }
  handleClick1(arg){

    if ( arg === 'final'){
      console.log("---",this.state.showFinal);
      this.setState({
        showFinal:true,
        showPersonal:false,
        showCommunication:false,
        showWorkflow:false
      })
     
    }
  }
  render(){
    let person = this.state.showPersonal ? <Person handleClick={(arg)=> this.handleClick(arg)}/> :null
    let commun = this.state.showCommunication ? <Communication handleClick1={(arg)=> this.handleClick1(arg)}/> :null
    let fin=this.state.showFinal ? <Final />: null
    let wflow=this.state.showWorkflow ? <Workflow handleClick={(arg)=> this.handleClick(arg)}/>: null
        
    return (
      <div >
          
        <div className="App">
            <div style={{backgroundColor:'blue',width:'100%',height:'50px'}}>
    {/* <img src={logo} style={{align:'center',height:'47px',width:'105px',paddingTop:'5px'}}></img>*/}
              
            </div>
            {wflow}
        </div>
           
        {person}
        {commun}
        {fin}
        </div>

        
    );
  }
  
}

export default App;
