import React,{Component} from 'react';
import './style.css';
import { TextField,Paper,Button,Switch} from '@material-ui/core';

const initialState ={
    address1:"",
    address2:"",
    landMark:"",
    city:"",
    state:"",
    pincode:"",
    contact:"",
    option:"",
    pEmail:"",
    cName:"",
    wEmail:"",
    pEmailError:"",
    pincodeError:"",
    wEmailError:"",
    contactError:"",

}


class Communication extends  Component{
    state= initialState;

    handleChange = event => {
        const isCheckbox = event.target.type === "checkbox";
        this.setState({
            [event.target.name]: isCheckbox ? event.target.checked :event.target.value

        });

    };

    validate = () => {
      
     if (this.state.pincode.length < 6 || this.state.pincode.length > 6){
          this.setState({pincodeError : " Invalid pincode"});  
          return false;
     }
        else {
          this.setState({pincodeError: " "}); 
      }
    if (this.state.contact.length < 10 || this.state.contact.length >10){
          this.setState({contactError : "Invalid phone Number "}); 
          return false;  
        } else {
          this.setState({contactError : " "});
        }
        if(!this.state.pEmail.includes("@") ||!this.state.pEmail.includes(".") ){
            this.setState({pEmailError : "Invalid Email "});
            return false;
        }else{
         this.setState({pEmailError : ""})
        }
        if(!this.state.wEmail.includes("@") ||!this.state.wEmail.includes(".") ){
            this.setState({wEmailError : "Invalid Email "});
            return false;
        }else{
         this.setState({wEmailError : ""})
        }
        this.handleClick1('final');
    }
    handleClick1=(arg) => {  
        this.props.handleClick1(arg);
       }
    
   handleSubmit = event => {  
       event.preventDefault();
      
   }
 render(){
       
        return(
            <div>
                <Paper elevation={1} style={{height:'900px',width:'1900px'}}>
 
             <div style={{paddingTop:'47px'}}>
                <Paper  elevation={3} class="MuiPaper-root MuiPaper-elevation3 MuiPaper-rounded box">
               
                <div>
                    <form onSubmit={this.handleSubmit}>
                    <table>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>AddressLine1<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text" name="address1" value={this.state.address1} onChange={this.handleChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>AddressLine2&nbsp;:</label>
                        </td>
                        <td>  
                        <TextField style={{width:'320px',marginLeft:'10px'}}  id="standard-required" type="text" 
                        name="address2" value={this.state.address2} onChange={this.handleChange} />
                        </td>
                        
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{ marginLeft:"20px"}}>LandMark<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text" name="landMark" value={this.state.landMark} onChange={this.handleChange} />
                        </td>
                        
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>City <sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField  style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text" name="city" value={this.state.city} onChange={this.handleChange} />
                        </td>
                        
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>State <sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField  style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text" name="state" value={this.state.state} onChange={this.handleChange} />
                        </td>
                        
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}} for="types">Pincode<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                      
                        <TextField  style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="number" name="pincode" value={this.state.pincode} onChange={this.handleChange} />
                        </td>
                        </tr>
                        <tr>
                            <td></td>
                        <td style={{color:'red',width:'40px',paddingLeft:'11px'}}>{this.state.pincodeError}</td>
                         </tr>
                      <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}} for="etypes">ContactNumber<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField  style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="number"  name="contact" value={this.state.contact} onChange={this.handleChange} />
                        </td>
                        </tr>
                        <tr>
                        <td></td>
                        <td style={{color:'red',width:'40px',paddingLeft:'11px'}}>{this.state.contactError}</td>
                         </tr>
                      <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>This Address is My<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                         <input type="radio" style={{marginLeft:'20px'}} name="option" value={this.state.option} onChange={this.handleChange} checked/><label>Residence</label>
                         <input type="radio" style={{marginLeft:'20px'}}  name="option" value={this.state.option} onChange={this.handleChange}/><label>Office</label>
                         </td>
                 </tr>
                 <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}} >Personal Email id<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField  style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="email" name="pEmail" value={this.state.pEmail} onChange={this.handleChange} />
                        </td>
                        </tr>
                        <tr>
                            <td></td>
                        <td style={{color:'red',width:'40px',paddingLeft:'11px'}}>{this.state.pEmailError}</td>
                         </tr>
                </table>
                <table>
                    <tr>
                    <td> <p style={{marginLeft:'19px'}}>Use office address for all communication</p></td><td> <Switch color="primary" inputProps={{ 'aria-label': 'primary checkbox' }} />
                      </td>
                       </tr>
                      
                    </table>
                    <p style={{marginLeft:'15px'}}> All Bank Communications will be sent to your Residential( Communication) address</p>
                      <table>
                      <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"18px"}} >Company Name<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField  style={{width:'320px',marginLeft:'15px'}} required id="standard-required" type="text" 
                        name="cName" value={this.state.cName} onChange={this.handleChange} />
                        </td>
                      </tr>
                      <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"18px"}} >Work Email id<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td >  
                        <TextField  style={{width:'320px',marginLeft:'15px'}} required id="standard-required" type="text" name="wEmail" value={this.state.wEmail} onChange={this.handleChange}  />
                        </td>
                       </tr>
                       <tr>
                        <td></td>
                        <td style={{color:'red',width:'40px',paddingLeft:'11px'}}>{this.state.wEmailError}</td>
                        </tr>
                        </table>  
                    
                          <Button variant="contained" style={{backgroundColor:'#429ef5',width:'190px',height:'30px',color:'#fff',marginLeft:'292px'}}  type="submit" onClick={(e)=>{this.validate()}}>
                         Continue  </Button>
                        
                    </form>
                </div>
                </Paper>
            </div>
          </Paper>
          
          </div>    
        )
    }
}

export default Communication;