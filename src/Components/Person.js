import React,{Component} from 'react';
import './style.css';
import { TextField, MenuItem, Select,Paper,Button} from '@material-ui/core';



const initialState ={
    firstName: "",
    lastName: "",
    Dob:"",
    residence:"",
    etype:"",
    pancard: "",
    file:"",
    firstNameError:"",
    lastNameError:"",
    dobError:"",
    pancardError: "",

}
class Person extends  Component{
    state= initialState;

    handleChange = event => {
        const isCheckbox = event.target.type === "checkbox";
        this.setState({
            [event.target.name]: isCheckbox ? event.target.checked :event.target.value

        });
       // const isValid = this.validate();

    };

    validate = () => {
      
      if(this.state.firstName.length <5 ){
       this.setState({firstNameError : "Should be atleast 5 characters "});
       return false;
       }
       else{ 
           this.setState({firstNameError : ""})
     }
      if (this.state.lastName.length < 5){
        this.setState({lastNameError : "Should be atleast 5 characters "});
        return false;
     }
      else {
        this.setState({lastNameError : " "}); 
    
      }

    if (this.state.pancard.length < 10 || this.state.pancard.length > 10){
        this.setState({pancardError : "Invalid PAN Number "});
        return false;   
      } else {
        this.setState({pancardError : " "}); 
         
      }
      this.handleClick('commun');

    }

    handleSubmit = event => {  
        event.preventDefault();
      
    };

    handleClick(arg) { 
        this.props.handleClick(arg);
         
     };
    

    
    render(){
        return(
            <div >
               <Paper elevation={1} style={{height:'900px',width:'1900px'}}>
                <div style={{paddingTop:'47px'}}>
                <span style={{color:'#42adf5',fontSize:'30px',textAlign:'left',marginLeft:'450px'}}>Check Loan Eligibility </span>
                </div>
                <div>
                    <span style={{textAlign:'left',marginLeft:'450px'}}><b>Lets calculate the amount you are eligible for</b></span>
                </div>
                <Paper  elevation={3} class="MuiPaper-root MuiPaper-elevation3 MuiPaper-rounded box">
                <div>
                    <form onSubmit={this.handleSubmit}>
                    <table>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>FirstName<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text"  name="firstName"  value={this.state.firstName} onChange={this.handleChange} />
                        </td>
                        </tr>
                        <tr>
                            <td></td>
                        <td style={{color:'red',width:'40px',paddingLeft:'11px'}}>{this.state.firstNameError}</td>
                   </tr>
                   
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>LastName<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text"  name="lastName" value={this.state.lastName} onChange={this.handleChange}/>
                        </td>
                        </tr>
                        <tr>
                        <td></td>
                        <td style={{color:'red',width:'40px',paddingLeft:'11px'}}>{this.state.lastNameError}</td>
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{ marginLeft:"20px"}}>Date Of Birth<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="date"  name="Dob" value={this.state.Dob}  onChange={this.handleChange} maxDate={new Date()} />
                        </td>
                        
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>City Of Residence<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField  style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text" name="residence" value={this.state.residence} onChange={this.handleChange}/>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}} for="types">Residence Type<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <Select style={{width:'320px',marginLeft:'10px'}} id="demo-simple-select"  name="select" value={this.state.select} onChange={this.handleChange}>
                       <MenuItem value={"CP"}>Company Provided</MenuItem>
                       <MenuItem value={"HO"}>Hostel</MenuItem>
                       <MenuItem value={'PG'}>Paying Guest</MenuItem>
                       <MenuItem value={"RSA"}>Rented-staying alone</MenuItem>
                       <MenuItem value={"RSF"}>Rented-staying with friends</MenuItem>
                       <MenuItem value={"RSFA"}>Rented-staying with family</MenuItem>
                       </Select>
                         
                        </td>
                      </tr>
                      </table>
                      
                          <h4 style={{marginLeft:"20px"}}>Employment and Income Details</h4>
                    
                      <table>
                      <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>Employment Type<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <Select style={{width:'320px',marginLeft:'10px'}} id="demo-simple-select" name="etype" value={this.state.etype} onChange={this.handleChange}>
                       <MenuItem value={"SE"}>Self Employed</MenuItem>
                       <MenuItem value={"SA"}>Salaried</MenuItem>
                       
                       </Select>
                        </td>
                      </tr>
                      <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>PanCard No<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <TextField  name="pancard" style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="text"  max="10" value={this.state.pancard} onChange={this.handleChange}/>
                        </td>
                     </tr>
                     <tr>
                        <td></td>
                        <td style={{color:'red',width:'40px',paddingLeft:'11px'}}>{this.state.pancardError}</td>
                   </tr>
                    <tr>
                        <td style={{textAlign:"left"}}>
                            <label style={{marginLeft:"20px"}}>Upload file<sup style={{color:'red'}}>*</sup>:</label>
                        </td>
                        <td>  
                        <input  style={{width:'320px',marginLeft:'10px'}} required id="standard-required" type="file" name="file" value={this.state.file} onChange={this.handleChange}/> 
                     </td>
                    </tr>
                    </table>
                    <table>
                    <tr>
                          <td><Button variant="contained" style={{backgroundColor:'#429ef5',width:'250px',height:'30px',color:'#fff',marginLeft:'235px'}} type="submit"  onClick={(e)=>{this.validate()}}>
                         Check Loan Eligibility
                        </Button></td>
                    </tr>
                    </table>
                    </form>
                </div>
                </Paper>
              </Paper> 
            </div>
              
        )
    }
}

export default Person;